.PHONY : develop 

develop :
	npx parcel src/index.pug

.PHONY : install

install :
	npx elm install mdgriffith/elm-markup
	npx elm install elm/http
	npx elm install elm-community/list-extra


.PHONY : verify-examples
verify-examples :
	rm -rf tests/VerifyExamples
	npx elm-verify-examples
	
.PHONY : test
test : verify-examples
	npx elm-test
