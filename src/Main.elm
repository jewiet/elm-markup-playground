module Main exposing (main)

import Custom.String as String
import Html exposing (Html)
import List.Extra as List
import Mark


main : Html Never
main =
    markup
        |> String.normalize
        |> markupToHtml


markupToHtml : String -> Html Never
markupToHtml input =
    case Mark.parse document input of
        Mark.Success parsed ->
            parsedToHtml parsed

        Mark.Almost partial ->
            Html.text "almost parsed"

        Mark.Failure errors ->
            Html.text "errors parsing"


parsedToHtml : Mark.Parsed -> Html Never
parsedToHtml parsed =
    case Mark.render document parsed of
        Mark.Success html ->
            html

        Mark.Almost partial ->
            Html.text "almost rendered"

        Mark.Failure errors ->
            Html.text "errors rendering"


document : Mark.Document (Html Never)
document =
    Mark.document
        Html.text
        Mark.string


markup : String
markup =
    """
    |> Title
        Fana is Awesome!
  
    Something about her makes me happy.
    """
