module Custom.String exposing (indentation, isBlank, normalize)

import List.Extra as List


{-| Dedents strings

    normalize "" --> ""

    normalize
      "  Fana is Awesome! And she is beautiful."
    --> "Fana is Awesome! And she is beautiful."

    normalize
      """
      Here is a text to test normalize function.
      It is insanely awesome!
      """
    --> "Here is a text to test normalize function.\nIt is insanely awesome!\n"

    normalize
      """
          Here is a text to test normalize function.
          It is insanely awesome!
      """
    --> "Here is a text to test normalize function.\nIt is insanely awesome!\n"

    normalize
      "\n\n        Here is a text to test normalize function.\n    It is insanely awesome!\n"
    --> "    Here is a text to test normalize function.\nIt is insanely awesome!\n"

-}
normalize : String -> String
normalize input =
    let
        dedent : Int
        dedent =
            indentation input
    in
    input
        |> String.split "\n"
        |> List.map (String.dropLeft dedent)
        |> List.dropWhile isBlank
        |> String.join "\n"


indentation : String -> Int
indentation input =
    input
        |> String.split "\n"
        |> List.filter (isBlank >> not)
        |> List.map String.toList
        |> List.map (List.takeWhile ((==) ' '))
        |> List.map List.length
        |> List.minimum
        |> Maybe.withDefault 0


{-| Check if a string doesn't have characters other than space

    isBlank "" --> True

    isBlank " Fana " --> False

    isBlank "      " --> True

    isBlank "\n" --> True

    isBlank "        \n\n\n " --> True

-}
isBlank : String -> Bool
isBlank input =
    String.trim input == ""
